﻿# Hawk

Hawk，Secure, simple key-value storage for OpenHarmonyOS

## 图片
<img src="art/screenshot.png" height="30%" width="30%"/>

### 引入方式
通过DevEco studio 2.0+,并下载SDK Native 版本2.0+
方式一
在entry的gradle中集成如下操作：
如果使用har,请将har放到entry中的libs
修改implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])增加*.har

如果引入lib中的har包，请引入 compile 'com.google.code.gson:gson:2.8.2'

```groovy
compile 'com.google.code.gson:gson:2.8.2'
```

方式二：
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:hawk:1.0.2'
```

### Initialize
```java
Hawk.init(context).build();
```
### Usage
Save any type (Any object, primitives, lists, sets, maps ...)
```java
Hawk.put(key, T);
```
Get the original value with the original type
```java
T value = Hawk.get(key);
```
Delete any entry
```java
Hawk.delete(key);
```
Check if any key exists
```java
Hawk.contains(key);
```
Check total entry count
```java
Hawk.count();
```
Get crazy and delete everything
```java
Hawk.deleteAll();
```

### More options
- Everything is pluggable, therefore you can change any layer with your custom implementation.
- NoEncryption implementation is provided out of box If you want to disable crypto.
```java
Hawk.init(context)
  .setEncryption(new NoEncryption())
  .setLogInterceptor(new MyLogInterceptor())
  .setConverter(new MyConverter())
  .setParser(new MyParser())
  .setStorage(new MyStorage())
  .build();
```
## Entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）


## License

Copyright 2016 Orhan Obut

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.