package com.orhanobut.hawk.inf;

import com.orhanobut.hawk.DataInfo;

/**
 * Intermediate layer that is used to serialize/deserialize the cipher text
 *
 * <p>Use custom implementation if built-in implementation is not enough.</p>
 *
 * @see Serializer
 */
public interface Serializer {
    /**
     * Serialize the cipher text along with the given data type
     * @param value value
     * @param cipherText text
     * @return serialized string
     */
    <T> String serialize(String cipherText, T value);

    /**
     * Deserialize the given text according to given DataInfo
     * @param plainText text
     * @return original object
     */
    DataInfo deserialize(String plainText);
}
