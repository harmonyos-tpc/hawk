package com.orhanobut.hawk;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.orhanobut.hawk.inf.Parser;

import ohos.agp.utils.TextTool;

import java.lang.reflect.Type;

/**
 * gson解析
 */
public final class GsonParser implements Parser {
    private final Gson gson;

    public GsonParser(Gson gson) {
        this.gson = gson;
    }

    @Override
    public <T> T fromJson(String content, Type type) throws JsonSyntaxException {
        if (TextTool.isNullOrEmpty(content)) {
            return null;
        }
        return gson.fromJson(content, type);
    }

    @Override
    public String toJson(Object body) {
        return gson.toJson(body);
    }
}
