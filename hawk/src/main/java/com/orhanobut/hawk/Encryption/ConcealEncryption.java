package com.orhanobut.hawk.Encryption;


/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.orhanobut.hawk.inf.Encryption;

import ohos.app.Context;

/**
 * 加密封装
 */
public class ConcealEncryption implements Encryption {
    public ConcealEncryption(Context context) {}

    @Override
    public boolean init() {
        return true;
    }

    @Override
    public String encrypt(String key, String plainText) throws Exception {
        return RC4.encryRc4String(key, plainText);
    }

    @Override
    public String decrypt(String key, String cipherText) throws Exception {
        return RC4.decryRc4(key, cipherText);
    }
}
