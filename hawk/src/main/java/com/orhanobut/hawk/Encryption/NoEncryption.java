package com.orhanobut.hawk.Encryption;

import com.orhanobut.hawk.inf.Encryption;

import java.nio.charset.Charset;
import java.util.Base64;

/**
 * Provides Base64 encoding as non-encryption option.
 * This doesn't provide any encryption
 */
public class NoEncryption implements Encryption {
    @Override
    public boolean init() {
        return true;
    }

    @Override
    public String encrypt(String key, String value) throws Exception {
        return encodeBase64(value.getBytes(Charset.defaultCharset()));
    }

    @Override
    public String decrypt(String key, String value) throws Exception {
        return new String(decodeBase64(value));
    }

    String encodeBase64(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    byte[] decodeBase64(String value) {
        return Base64.getDecoder().decode(value);
    }
}
