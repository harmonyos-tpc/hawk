package com.huawei.hawk;

import com.huawei.hawk.slice.MainAbilitySlice;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.inf.LogInterceptor;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends Ability {

    private static final String TAG_LOG = "HawkLog";

    private static final int DOMAIN_ID = 0xD000F00;

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, DOMAIN_ID, TAG_LOG);
    private static final String LOG_FORMAT = "%{public}s: %{public}s";
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);


        timeHawkInit();
        timeHawkPut();
        timeHawkGet();
        timeHawkContains();
        timeHawkCount();
        timeHawkDelete();
    }

    private void timeHawkInit() {
        long startTime = System.currentTimeMillis();

        Hawk.init(this).setLogInterceptor(new LogInterceptor() {
            @Override public void onLog(String message) {
                dLog(message);
            }
        }).build();

        long endTime = System.currentTimeMillis();
        dLog("Hawk.init: " + (endTime - startTime) + "ms");
    }

    private void timeHawkPut() {
        long startTime = System.currentTimeMillis();

        Hawk.put("key", "value");

        long endTime = System.currentTimeMillis();
        dLog("Hawk.put: " + (endTime - startTime) + "ms");
    }

    private void timeHawkGet() {
        long startTime = System.currentTimeMillis();

        Hawk.get("key");

        long endTime = System.currentTimeMillis();
        dLog("Hawk.get: " + (endTime - startTime) + "ms");
    }

    private void timeHawkCount() {
        long startTime = System.currentTimeMillis();

        Hawk.count();

        long endTime = System.currentTimeMillis();
        dLog("Hawk.count: " + (endTime - startTime) + "ms");
    }

    private void timeHawkContains() {
        long startTime = System.currentTimeMillis();

        Hawk.contains("key");

        long endTime = System.currentTimeMillis();
        dLog("Hawk.count: " + (endTime - startTime) + "ms");
    }

    private void timeHawkDelete() {
        long startTime = System.currentTimeMillis();

        Hawk.delete("key");

        long endTime = System.currentTimeMillis();
        dLog("Hawk.count: " + (endTime - startTime) + "ms");
    }

    public void dLog(String str) {
        HiLog.info(LABEL_LOG, LOG_FORMAT,TAG_LOG, str);
    }
}
